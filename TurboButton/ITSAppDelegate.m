//
//  ITSAppDelegate.m
//  TurboButton
//
//  Created by Jim Zajkowski on 8/13/14.
//  Copyright (c) 2014 University of Michigan. All rights reserved.
//

#import "ITSAppDelegate.h"

@interface ITSAppDelegate ()

// The timer
@property (retain) NSTimer *countdownTimer;
@property (retain) NSDate *launchAt;
@property (retain) NSURL *urlToLaunch;

@property (unsafe_unretained) IBOutlet NSWindow *preferencesWindow;
- (IBAction)openPreferences:(id)sender;

// Main window
@property (weak) IBOutlet NSTextField *launchingLabel;
@property (weak) IBOutlet NSTextField *countdownTimerLabel;
- (IBAction)launchNow:(id)sender;

// Preferences Window
@property (weak) IBOutlet NSTextField *preferencesTimeLabel;
@property (weak) IBOutlet NSSlider *preferencesTimeSlider;

- (IBAction)preferencesTimeSliderChanged:(id)sender;
- (IBAction)preferencesFindThingToRunButton:(id)sender;

@end

@implementation ITSAppDelegate

- (void)awakeFromNib
{
    self.launchingLabel.stringValue = @"";
    self.countdownTimerLabel.stringValue = @"";
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{ @"seconds": @(5) }];
    
    NSData* bookmarkData = [[NSUserDefaults standardUserDefaults] objectForKey:@"bookmark"] ;
    self.urlToLaunch = [NSURL URLByResolvingBookmarkData:bookmarkData
                                                 options:0
                                           relativeToURL:nil
                                     bookmarkDataIsStale:nil
                                                   error:nil];
    // Is there a thing for us to do?
    if (! self.urlToLaunch )
    {
        [self openPreferences:self];
    }
    else
    {
        NSInteger delay = [[NSUserDefaults standardUserDefaults] integerForKey:@"seconds"];
        
        self.launchingLabel.stringValue = [NSString stringWithFormat:@"Launching %@ after %ld seconds...", self.urlToLaunch.path, delay];
        
        self.launchAt = [NSDate dateWithTimeIntervalSinceNow:delay];
        self.countdownTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(tick:) userInfo:nil repeats:YES];
    }
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

- (void)launchURL
{
    [[NSWorkspace sharedWorkspace] openURL:self.urlToLaunch];
    [self.countdownTimer invalidate];
    [[NSApplication sharedApplication] terminate:self];
}

- (void)tick:(NSTimer *)timer
{
    NSDate *now = [NSDate date];
    NSInteger diff = (NSInteger)[self.launchAt timeIntervalSinceDate:now];
    
    int seconds = diff % 60;
    int minutes = (diff / 60) % 60;
    
    self.countdownTimerLabel.stringValue = [NSString stringWithFormat:@"%2d:%02d", minutes, seconds];
    
    if (diff <= 0)
    {
        [self launchURL];
    }
}

- (IBAction)preferencesTimeSliderChanged:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@(self.preferencesTimeSlider.integerValue) forKey:@"seconds"];
    
    NSInteger numberOfSeconds = self.preferencesTimeSlider.integerValue;
    NSString *timeInWords;
    
    int seconds = numberOfSeconds % 60;
    int minutes = (numberOfSeconds / 60) % 60;
    
    if (minutes > 0)
    {
        if (seconds == 0)
        {
            timeInWords = [NSString stringWithFormat:@"%d minutes", minutes];
        }
        else
        {
            timeInWords = [NSString stringWithFormat:@"%d minutes, %d seconds", minutes, seconds];
        }
    }
    else
    {
        timeInWords = [NSString stringWithFormat:@"%d seconds", seconds];
    }
    
    self.preferencesTimeLabel.stringValue = timeInWords;

}

- (IBAction)preferencesFindThingToRunButton:(id)sender
{
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];

    [openPanel beginSheetModalForWindow:self.preferencesWindow completionHandler:^(NSInteger result) {

        if (openPanel.URL)
        {
            NSData* bookmarkData = [openPanel.URL bookmarkDataWithOptions:0
                                           includingResourceValuesForKeys:nil
                                                            relativeToURL:nil
                                                                    error:nil];
            
            [[NSUserDefaults standardUserDefaults] setObject:bookmarkData forKey:@"bookmark"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }];
}

- (IBAction)launchNow:(id)sender
{
    [self launchURL];
}

- (IBAction)openPreferences:(id)sender
{
    // Stop the violins!
    [self.countdownTimer invalidate];
    self.countdownTimer = nil;
    
    // Bump the slider to the right value
    [self.preferencesTimeSlider setIntegerValue:[[NSUserDefaults standardUserDefaults] integerForKey:@"seconds"]];

    [self preferencesTimeSliderChanged:nil];
    [self.preferencesWindow makeKeyAndOrderFront:self];
}

- (void)windowWillClose:(NSNotification *)notification
{
    // Re-run the app logic when the prefs window closes
    if (notification.object == self.preferencesWindow) {
        [self applicationDidFinishLaunching:nil];
    }
}

@end
