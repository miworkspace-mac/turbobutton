//
//  main.m
//  TurboButton
//
//  Created by Jim Zajkowski on 8/13/14.
//  Copyright (c) 2014 University of Michigan. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
