//
//  ITSAppDelegate.h
//  TurboButton
//
//  Created by Jim Zajkowski on 8/13/14.
//  Copyright (c) 2014 University of Michigan. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ITSAppDelegate : NSObject <NSApplicationDelegate, NSOpenSavePanelDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
